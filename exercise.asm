#include p16f877a.inc
    org 00
    cblock 0x70
	W_TEMP
	STATUS_TEMP
    endc
    goto start
    org 04
    goto ISR
    
    start 
    PUSH MACRO 
	MOVWF W_TEMP 
	SWAPF STATUS,W 
	MOVWF STATUS_TEMP 
    ENDM
    POP MACRO 
	SWAPF STATUS_TEMP,W 
	MOVWF STATUS 
	SWAPF W_TEMP,F 
	SWAPF W_TEMP,W 
    ENDM 
    
    banksel TRISC 
    clrf TRISC
    movlw 0xff
    movwf TRISD
    movwf TRISB
    
    banksel INTCON
    movlw b'10010000'
    movwf INTCON
    banksel OPTION_REG
    movlw b'01000000'
    movwf OPTION_REG
    loop
    goto loop
    
    ISR
        PUSH
	banksel PORTD
	movf PORTD,w
	movwf PORTC
	POP
	bcf INTCON,1
    retfie
    
    end